import java.util.Arrays;

public class CalculationUtils {

    private final static double EPSILON = 0.000001d;

    public static double[] solve(Double a, Double b, Double c) {
        for (Double value : Arrays.asList(a, b, c)) {
            if (value == null) {
                throw new IllegalArgumentException("Параметры не могут быть null");
            }
            if (value.isNaN()) {
                throw new IllegalArgumentException("Параметры не могут быть Nan");
            }
            if (value.isInfinite()) {
                throw new IllegalArgumentException("Параметры не могут быть Infinite");
            }
        }
        if (isZero(a)) {
            throw new IllegalArgumentException("Значение a не должно быть равно 0");
        }
        double d = b * b - 4 * a * c;
        if (d > 0) {
            double x1 = (-b - Math.sqrt(d)) / (2 * a);
            double x2 = (-b + Math.sqrt(d)) / (2 * a);
            return new double[]{x1, x2};
        } else if (isZero(d)) {
            double x = -b / (2 * a);
            return new double[]{x};
        } else {
            return new double[]{};
        }
    }

    private static boolean isZero(double x) {
        return Math.abs(x) < EPSILON;
    }
}
