import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CalculationUtilsTest {

    @Test
    public void solve_zero() {
        double[] actual = CalculationUtils.solve(1.0, 0.0, 1.0);
        assertNotNull(actual);
        assertEquals(0, actual.length);
    }

    @Test
    public void solve_two() {
        double[] actual = CalculationUtils.solve(1.0, 0.0, -1.0);
        assertNotNull(actual);
        assertEquals(2, actual.length);
        assertEquals(-1, actual[0]);
        assertEquals(1, actual[1]);
    }

    @Test
    public void solve_one() {
        double[] actual = CalculationUtils.solve(1.0, 2.0, 1.0000000001);
        assertNotNull(actual);
        assertEquals(1, actual.length);
        assertEquals(-1, actual[0]);
    }

    @Test
    public void solve_zeroAValue() {
        assertThrows(
                IllegalArgumentException.class,
                () -> CalculationUtils.solve(0.0, 4.0, 5.0)
        );
    }

    @Test
    public void solve_nullValue() {
        Double value = null;
        assertThrows(
                IllegalArgumentException.class,
                () -> CalculationUtils.solve(value, 1.0, 1.0)
        );
    }

    @Test
    public void solve_nanValue() {
        Double value = Double.longBitsToDouble(0x7ff8000000000000L);
        assertTrue(value.isNaN());
        assertThrows(
                IllegalArgumentException.class,
                () -> CalculationUtils.solve(1.0, 1.0, value)
        );
    }

    @Test
    public void solve_infiniteValue() {
        Double value = Double.POSITIVE_INFINITY;
        assertTrue(value.isInfinite());
        assertThrows(
                IllegalArgumentException.class,
                () -> CalculationUtils.solve(1.0, value, 1.0)
        );
    }
}
